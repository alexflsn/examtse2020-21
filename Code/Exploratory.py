# Part 1 : Determine Probabilities

# Import libraries
import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import datetime

# Style ggplot for the plots
matplotlib.style.use('ggplot')

# Import the dataset
path = os.path.abspath("../Data/Coffeebar_2016-2020.csv")
df = pd.read_csv(path, sep=";")

# Section 1
# What food and drinks are sold by the coffee bar? How many unique customers did the bar have?

print(df.describe())  # Description of our dataset
print(df.DRINKS.unique())  # Different drinks sold by the coffee bar
print(df.FOOD.unique())  # Different food sold by the coffee bar
print(df.CUSTOMER.nunique())  # Unique customers

# Section 2
# Make a bar plot of the total amount of sold foods (plot1) and drinks (plot2) over the five years

df['TIME'] = pd.to_datetime(df['TIME'])  # Declare the time variable
df['YEAR'] = pd.DatetimeIndex(df['TIME']).year  # Create the variable YEAR
YEAR = np.unique(df.YEAR)  # Get unique values for years

# Plot1 : Total amount of sold foods
df_food = df.groupby(['YEAR']).FOOD.count().reset_index(name="NUMBER_FOOD")  # Count the number of sold food per year
bar = plt.bar(YEAR, df_food.NUMBER_FOOD)  # Make a barplot
plt.title('Plot 1 : Total amount of sold foods per year', fontsize=20)
plt.ylim(32500, 33200)
plt.yticks(np.arange(32500, 33200, 100))
plt.xlabel("Year", size=14)
plt.ylabel("Total amount of sold foods", size=14)
for rect in bar:  # Write the amount of sold food above the bars
    height = rect.get_height()
    plt.text(rect.get_x() + rect.get_width() / 2.0, height, '%d' % int(height), ha='center', va='bottom')

# Save Plot1
plt.savefig("../Results/Plot1.png", bbox_inches='tight')

# Clear
plt.clf()

# Plot2 : Total amount of sold drinks
df_drinks = df.groupby(['YEAR']).DRINKS.count().reset_index(
    name="NUMBER_DRINKS")  # Count the number of sold drinks per year

bar1 = plt.bar(YEAR, df_drinks.NUMBER_DRINKS)  # Make a barplot
plt.title('Plot 2 : Total amount of sold drinks per year', fontsize=20)
plt.xlabel('Year', fontsize=18)
plt.ylabel('Total amount of sold drinks', fontsize=16)
for rect in bar1:  # Write the amount of sold drinks above the bars
    height = rect.get_height()
    plt.text(rect.get_x() + rect.get_width() / 2.0, height, '%d' % int(height), ha='center', va='bottom')

# Save Plot2
plt.savefig("../Results/Plot2.png", bbox_inches='tight')

# Clear
plt.clf()

# Additional plots : Stacked bar chart of the total amount of sold foods and drinks

# Number of sold drinks and foods by type across the years
df_drinks_detail = df.groupby(['YEAR', 'DRINKS']).DRINKS.count().reset_index(
    name="NUMBER_DRINKS")  # Name the last column
df_food_detail = df.groupby(['YEAR', 'FOOD']).FOOD.count().reset_index(
    name="NUMBER_FOOD")  # Name the last column

# Additional plot 3: Stacked barplot with the number of each type of food sold

# Create dataframe containing the number of each food sold per year
COOKIE = df_food_detail[df_food_detail.FOOD == "cookie"]
MUFFIN = df_food_detail[df_food_detail.FOOD == "muffin"]
PIE = df_food_detail[df_food_detail.FOOD == "pie"]
SANDWICH = df_food_detail[df_food_detail.FOOD == "sandwich"]

# Extract the last column containing the amount for each year
NB_COOKIE = COOKIE["NUMBER_FOOD"].reset_index(name="TOTAL")
NB_PIE = PIE["NUMBER_FOOD"].reset_index(name="TOTAL")
NB_MUFFIN = MUFFIN["NUMBER_FOOD"].reset_index(name="TOTAL")
NB_SANDWICH = SANDWICH["NUMBER_FOOD"].reset_index(name="TOTAL")

# Height of Cookie + Pie
height1 = NB_COOKIE["TOTAL"] + NB_PIE["TOTAL"]
# Height of Cookie + Pie + Muffin
height2 = NB_COOKIE["TOTAL"] + NB_PIE["TOTAL"] + NB_MUFFIN["TOTAL"]

# Position of the bars on the x-axis
r = range(len(YEAR))

# Create a stacked barplot with the number of each type of food sold
fig, ax = plt.subplots()
plot1 = ax.bar(YEAR, NB_COOKIE["TOTAL"], label='Cookie')
plot2 = ax.bar(YEAR, NB_PIE["TOTAL"], label='Pie', bottom=NB_COOKIE["TOTAL"])
plot3 = ax.bar(YEAR, NB_MUFFIN["TOTAL"], label='Muffin', bottom=height1)
plot4 = ax.bar(YEAR, NB_SANDWICH["TOTAL"], label='Sandwich', bottom=height2)

ax.set_ylabel('Total amount of sold food')
ax.set_xlabel('Year')
ax.set_title('Plot 3 : Total amount of each sold food per year')
ax.set_ylim(0, 33200)
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

# Write the amount sold for each product
for r1, r2, r3, r4 in zip(plot1, plot2, plot3, plot4):
    h1 = r1.get_height()
    h2 = r2.get_height()
    h3 = r3.get_height()
    h4 = r4.get_height()
    plt.text(r1.get_x() + r1.get_width() / 2., h1 / 2., "%d" % h1, ha="center", va="center", color="white", fontsize=14,
             fontweight="bold")
    plt.text(r2.get_x() + r2.get_width() / 2., h1 + h2 / 2., "%d" % h2, ha="center", va="center", color="white",
             fontsize=14, fontweight="bold")
    plt.text(r3.get_x() + r3.get_width() / 2., h1 + h2 + h3 / 2., "%d" % h3, ha="center", va="center", color="white",
             fontsize=14, fontweight="bold")
    plt.text(r4.get_x() + r3.get_width() / 2., h1 + h2 + h3 + h4 / 2., "%d" % h4, ha="center", va="center",
             color="white", fontsize=14, fontweight="bold")

# Save the plot
plt.savefig("../Results/Plot3.png", bbox_inches='tight')

# Clear
plt.clf()

# Additional plot 4: Stacked barplot with the number of each type of drink sold
COFFEE = df_drinks_detail[df_drinks_detail.DRINKS == "coffee"]

# Create dataframe containing the number of each drink sold per year
FRAPPUCINO = df_drinks_detail[df_drinks_detail.DRINKS == "frappucino"]
MILKSHAKE = df_drinks_detail[df_drinks_detail.DRINKS == "milkshake"]
SODA = df_drinks_detail[df_drinks_detail.DRINKS == "soda"]
TEA = df_drinks_detail[df_drinks_detail.DRINKS == "tea"]
WATER = df_drinks_detail[df_drinks_detail.DRINKS == "water"]

# Extract the last column containing the amount for each year
NB_COFFEE = COFFEE["NUMBER_DRINKS"].reset_index(name="TOTAL")
NB_FRAPPUCINO = FRAPPUCINO["NUMBER_DRINKS"].reset_index(name="TOTAL")
NB_MILKSHAKE = MILKSHAKE["NUMBER_DRINKS"].reset_index(name="TOTAL")
NB_SODA = SODA["NUMBER_DRINKS"].reset_index(name="TOTAL")
NB_TEA = TEA["NUMBER_DRINKS"].reset_index(name="TOTAL")
NB_WATER = WATER["NUMBER_DRINKS"].reset_index(name="TOTAL")

# Height of Coffee + Frappucino
height3 = NB_COFFEE["TOTAL"] + NB_FRAPPUCINO["TOTAL"]
# Height of Coffee + Frappucino + Milkshake
height4 = NB_COFFEE["TOTAL"] + NB_FRAPPUCINO["TOTAL"] + NB_MILKSHAKE["TOTAL"]
# Height of Coffee + Frappucino + Milkshake + Soda
height5 = NB_COFFEE["TOTAL"] + NB_FRAPPUCINO["TOTAL"] + NB_MILKSHAKE["TOTAL"] + NB_SODA["TOTAL"]
# Height of Coffee + Frappucino + Milkshake + Soda + Tea
height6 = NB_COFFEE["TOTAL"] + NB_FRAPPUCINO["TOTAL"] + NB_MILKSHAKE["TOTAL"] + NB_SODA["TOTAL"] + NB_TEA["TOTAL"]

# Position of the bars on the x-axis
r = range(len(YEAR))

# Create a stacked barplot with the number of each type of drink sold
fig2, ax = plt.subplots()
plot5 = ax.bar(YEAR, NB_COFFEE["TOTAL"], label='Coffee')
plot6 = ax.bar(YEAR, NB_FRAPPUCINO["TOTAL"], label='Frappucino', bottom=NB_COFFEE["TOTAL"])
plot7 = ax.bar(YEAR, NB_MILKSHAKE["TOTAL"], label='Milkshake', bottom=height3)
plot8 = ax.bar(YEAR, NB_SODA["TOTAL"], label='Soda', bottom=height4)
plot9 = ax.bar(YEAR, NB_TEA["TOTAL"], label='Tea', bottom=height5)
plot10 = ax.bar(YEAR, NB_WATER["TOTAL"], label='Water', bottom=height6)

ax.set_ylabel('Total amount of sold drinks')
ax.set_xlabel('Year')
ax.set_title('Plot 4 : Total amount of each sold drinks per year')
ax.set_ylim(0, 65000)
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

# Write the amount sold for each product
for r5, r6, r7, r8, r9, r10 in zip(plot5, plot6, plot7, plot8, plot9, plot10):
    h5 = r5.get_height()
    h6 = r6.get_height()
    h7 = r7.get_height()
    h8 = r8.get_height()
    h9 = r9.get_height()
    h10 = r10.get_height()

    plt.text(r5.get_x() + r5.get_width() / 2., h5 / 2., "%d" % h5, ha="center", va="center", color="white", fontsize=14,
             fontweight="bold")
    plt.text(r6.get_x() + r6.get_width() / 2., h5 + h6 / 2., "%d" % h6, ha="center", va="center", color="white",
             fontsize=14, fontweight="bold")
    plt.text(r7.get_x() + r7.get_width() / 2., h5 + h6 + h7 / 2., "%d" % h7, ha="center", va="center", color="white",
             fontsize=14, fontweight="bold")
    plt.text(r8.get_x() + r8.get_width() / 2., h5 + h6 + h7 + h8 / 2., "%d" % h8, ha="center", va="center",
             color="white", fontsize=14, fontweight="bold")
    plt.text(r9.get_x() + r9.get_width() / 2., h5 + h6 + h7 + h8 + h9 / 2., "%d" % h9, ha="center", va="center",
             color="white",
             fontsize=14, fontweight="bold")
    plt.text(r10.get_x() + r10.get_width() / 2., h5 + h6 + h7 + h8 + h9 + h10 / 2., "%d" % h10, ha="center",
             va="center", color="white", fontsize=14, fontweight="bold")

# Save the plot
plt.savefig("../Results/Plot4.png", bbox_inches='tight')

# Clear
plt.clf()

# Section 3
# Determine the average that a customer buys a certain food or drink at any given time
df['HOUR'], df['DAY'] = df['TIME'].apply(lambda x: x.time()), df['TIME'].apply(lambda x: x.date())

# Create a dataframe with the number of certain drinks bought at a given time
df1 = df.groupby(['HOUR', 'DRINKS']).DRINKS.count().reset_index(name='OCCURRENCES')
df1 = df1.pivot_table('OCCURRENCES', ['HOUR'], 'DRINKS')

# Create variable that sums the total number of drinks sold at a given time
df1.loc[:, 'TOTAL_DRINKS'] = df1.sum(numeric_only=True, axis=1)

# Compute the probabilities to buy a certain drink at a given time
df1['PROBABILITY_COFFEE'] = df1['coffee'] / df1['TOTAL_DRINKS']
df1['PROBABILITY_FRAPPUCCINO'] = df1['frappucino'] / df1['TOTAL_DRINKS']
df1['PROBABILITY_MILKSHAKE'] = df1['milkshake'] / df1['TOTAL_DRINKS']
df1['PROBABILITY_SODA'] = df1['soda'] / df1['TOTAL_DRINKS']
df1['PROBABILITY_TEA'] = df1['tea'] / df1['TOTAL_DRINKS']
df1['PROBABILITY_WATER'] = df1['water'] / df1['TOTAL_DRINKS']

# Create food called nothing if the customer did not buy something to eat
df['FOOD'] = df['FOOD'].fillna('nothing')

# Create a dataframe with the number of certain food bought at a given time
df2 = df.groupby(['HOUR', 'FOOD']).FOOD.count().reset_index(name='OCCURRENCES')
df2 = df2.pivot_table('OCCURRENCES', ['HOUR'], 'FOOD').fillna(0)

# Create variable that sums the total number of food sold at a given time
df2['TOTAL_FOOD'] = df2['cookie'] + df2['muffin'] + df2['pie'] + df2['sandwich'] + df2['nothing']

# Compute the probabilities to buy a certain type of food at a given time
df2['PROBABILITY_COOKIE'] = df2['cookie'] / df2['TOTAL_FOOD']
df2['PROBABILITY_MUFFIN'] = df2['muffin'] / df2['TOTAL_FOOD']
df2['PROBABILITY_PIE'] = df2['pie'] / df2['TOTAL_FOOD']
df2['PROBABILITY_SANDWICH'] = df2['sandwich'] / df2['TOTAL_FOOD']
df2['PROBABILITY_NOTHING'] = df2['nothing'] / df2['TOTAL_FOOD']

# Save the dataframes
df1.to_csv('../Results/Probabilities_Drinks.csv', sep=";", header=True)
df2.to_csv('../Results/Probabilities_Food.csv', sep=";", header=True)

path1 = os.path.abspath("../Results/Probabilities_Drinks.csv")
df1 = pd.read_csv(path1, sep=";")
path2 = os.path.abspath("../Results/Probabilities_Food.csv")
df2 = pd.read_csv(path2, sep=";")

# Display sentences for the probabilities of buying drinks

# Display the probabilities in percentage and round it by 2 decimals
df1.loc[:, ['PROBABILITY_COFFEE', 'PROBABILITY_FRAPPUCCINO', 'PROBABILITY_TEA', 'PROBABILITY_WATER',
            'PROBABILITY_SODA', 'PROBABILITY_MILKSHAKE']] = df1.loc[:, ['PROBABILITY_COFFEE',
                                                                        'PROBABILITY_FRAPPUCCINO', 'PROBABILITY_TEA',
                                                                        'PROBABILITY_WATER', 'PROBABILITY_SODA',
                                                                        'PROBABILITY_MILKSHAKE']].values * 100
df1 = df1.round(decimals=2)

# Write a sentence for each hour
for i in range(len(df1)):
    print("On average, the probability of a customer at", df1.loc[i, "HOUR"], "buying coffee is",
          df1.loc[i, "PROBABILITY_COFFEE"], "%, buying soda is", df1.loc[i, "PROBABILITY_SODA"],
          "%, buying water is", df1.loc[i, "PROBABILITY_WATER"], "%, buying frappucino is",
          df1.loc[i, "PROBABILITY_FRAPPUCCINO"], "%, buying tea is", df1.loc[i, "PROBABILITY_TEA"],
          "% and buying milkshake is", df1.loc[i, "PROBABILITY_MILKSHAKE"], "%.")

# Display sentences for the probabilities of buying food

# Display the probabilities in percentage and round it by 2 decimals
df2.loc[:, ['PROBABILITY_COOKIE', 'PROBABILITY_MUFFIN', 'PROBABILITY_PIE',
            'PROBABILITY_SANDWICH', 'PROBABILITY_NOTHING']] = df2.loc[:, ['PROBABILITY_COOKIE',
                                                                          'PROBABILITY_MUFFIN', 'PROBABILITY_PIE',
                                                                          'PROBABILITY_SANDWICH',
                                                                          'PROBABILITY_NOTHING']].values * 100
df2 = df2.round(decimals=2)

# Write a sentence for each hour
for i in range(len(df2)):
    print("On average, the probability of a customer at", df2.loc[i, "HOUR"], "buying cookie is",
          df2.loc[i, "PROBABILITY_COOKIE"], "%, buying muffin is", df2.loc[i, "PROBABILITY_MUFFIN"], "%, buying pie is",
          df2.loc[i, "PROBABILITY_PIE"], "%, buying sandwich is", df2.loc[i, "PROBABILITY_SANDWICH"],
          "% and buying nothing is", df2.loc[i, "PROBABILITY_NOTHING"], "%.")

# Save the dataframes with the probabilities
df1.to_csv(path1, sep=";", header=True)
df2.to_csv(path2, sep=";", header=True)

# Export the main dataset in the file export.csv
df1['HOUR'] = df1['HOUR'].astype(str)
df['HOUR'] = df['HOUR'].astype(str)
df = df.merge(df1, on='HOUR', how='left')
df = df.merge(df2, on='HOUR', how='left')

exportpath = os.path.abspath('../Results/export.csv')
df.to_csv(exportpath, sep=";", index=False)
