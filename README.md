# ExamTSE2020-21
This exam project models a coffee bar that sells drinks and food. It is splitted
into 4 parts. The first one investigates a pre-existing data set and determines some probabilities. 
The second part focuses on creating more realistic customers.
The third one is about simulations. Finally, the last part tries to improve and adjust the code.

In this file you will find some information on the content of this repository, as
well as the prerequisites to run the codes.

## Prerequisites
Make you sure you have installed version 3.8 of Python.
You can use PyCharm as an IDE to run the code if you wish.
The following libraries are needed :
```
import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import collections
import datetime
import random
import time
import statistics
```

## Installation
To run the code, you first need to clone this repository to PyCharm using
this model :
```
 git clone https://github.com/your_username_/Project-Name.git
```
## Description of the Project
### Code directory
The Code directory contains a file with the code related to Part 1 of the project
called "Exploratory.py". It contains the different steps to construct bar plots 
and summary statistics along with the computation of the probabilities that a 
customer buys a certain food or drink at any given time.
The code related to the second and third part of the project is in the files "Theoretical.py" and 
"Simulations.py". The first one contains the code to define the rules when a 
consumer buys and the second one is the implementation part where we will do simulations. 
The file "Theoretical_initial.py" is our first try for the second part.
In the file "AddPlotsPart3.py" are presented the additional plots to describe our simulation that are required 
in the third part of the project.
Finally, in the file "AddQuestionsPart4", we answer to additional questions in order to check the efficiency of our code.

### Data directory
The Data directory contains a csv file called "Coffeebar_2016-2020.csv". It
contains five years data on what customers bought at a given time of the day.
This file has been forked on the repository of GertDeGeyterToulouse.

### Documentation directory
The Documentation directory contains a Power Point file that summarizes the 
instruction for the Project.

### Results Directory
The Results directory contains some plots obtained by running Exploratory.py, AddPlotsPart3.py and AddQuestionsPart4.py codes.
It also contains data sets in csv format.

The files :

- "export.csv" is the initial dataset with choice probabilities added.

- "Probabilities_Drinks" and "Probabilities_Food" contains the according choice probability for each hour of a day.

- "simulation.csv" is the result of part 3 simulation. 

- "simulation_question3", "simulation_question4", "simulation_question5" and "simulation_question6" refer to the different simulations of part 4.

- "buying_history1.csv" and "buying_history2.csv" are the files used to answer question 1 of part 4.

The plots : 

- Plot1 to Plot4 are bar plots linked to Part 1 of the project.

- Plot5 to Plot8 are the plots done for part 3. It includes plots of the average daily profit for the simulation and for the initial dataset.

- Plot9 to Plot 12 represents the average daily profit at the end of the different simulations done in part 4.

## Contribution
If you want to contribute to this project :
Fork this repository and create your feature branch. Then , commit and push your changes.


## Authors
* **Alexandra Florisson**
* **Camille Luis**

