# Part 3 : Simulations

# Section 2 : Additional Plots

# Import the libraries
import pandas as pd
import os
import numpy as np
import random
from pandas import DataFrame
import collections
import datetime
import matplotlib.pyplot as plt
import matplotlib
import statistics

# Import the initial data set with hours and years and the results from the simulation
path = os.path.abspath('../Results/export.csv')
df = pd.read_csv(path, sep=";")
path2 = os.path.abspath('../Results/simulation.csv')
result = pd.read_csv(path2, sep=";", low_memory=False)

# Plot 5 : Total amount of money spent for each type of customer (for the simulation)
tot_amount1 = result.groupby(["SUBTYPE"])["TOTAMOUNTPAID"].sum().reset_index(name="TOT")
print(tot_amount1)
bar = plt.bar(tot_amount1.SUBTYPE, tot_amount1.TOT)
plt.title('Plot 5 : Total amount spent by each type of customer at the end of the simulation', fontsize=20)
plt.xlabel("Customer type", size=14)
plt.ylabel("Total amount spent in millions of euros", size=14)
for rect in bar:  # Write the average amount spent above the bars
    height = rect.get_height()
    plt.text(rect.get_x() + rect.get_width() / 2.0, height, '%d' % float(height), ha='center', va='bottom')
# plt.savefig("../Results/Plot5.png", bbox_inches='tight')
plt.clf()

# Count the number of customers by subtype
print(len(result[result['SUBTYPE'] == 'Hipster']))
print(len(result[result['SUBTYPE'] == 'Regular1']))
print(len(result[result['SUBTYPE'] == 'Regular2']))
print(len(result[result['SUBTYPE'] == 'Tripadvisor']))

# Count number of regular2 and hipster to check if they stopped to come at some point
pd.set_option('display.max_rows', None)
result['DAY'] = pd.to_datetime(result['DAY'])
result['MONTH'] = result.DAY.dt.to_period("M")
print(result.groupby(["MONTH", "SUBTYPE"]).DRINK.count())
pd.reset_option('display.max_rows')

# Plot 6 : Average daily profit for the simulation (without tips)
plot2 = result.groupby(["DAY"])["AMOUNTPAID"].sum().reset_index(name="TOT2")
plot2['DAY'] = pd.to_datetime(plot2['DAY'])  # Declare the time variable
print(plot2)
print(statistics.mean(plot2.TOT2))  # Compute the average profit
plt.plot(plot2.DAY, plot2.TOT2)
plt.title('Plot 6 : Average daily profit at the end of the simulation', fontsize=20)
plt.xlabel("Years", size=14)
plt.ylabel("Profit in euros", size=14)
# plt.savefig("../Results/Plot6.png", bbox_inches='tight')
plt.clf()

# Plot 7 : Average daily profit for the initial dataset (no tips)
# Replacing na values in food with "nothing"
df['FOOD'].fillna("nothing", inplace=True)

# Add the prices of the drinks and food to the initial dataset
food_dictionary = {'sandwich': 2, 'cookie': 2, 'muffin': 3, 'pie': 3, 'nothing': 0}
drink_dictionary = {'milkshake': 5, 'frappucino': 4, 'water': 2, 'coffee': 3, 'soda': 3, 'tea': 3}
df['PRICEF'] = df['FOOD'].map(food_dictionary)
df['PRICED'] = df['DRINKS'].map(drink_dictionary)

# Sum of income received at each time slot
df['PROFIT'] = df["PRICEF"] + df["PRICED"]

# Plot the profit of the initial dataset over 5 years
plot3 = df.groupby(['DAY'])["PROFIT"].sum().reset_index(name="PROFIT")
plot3['DAY'] = pd.to_datetime(plot3['DAY'])  # Declare the time variable
print(statistics.mean(plot3.PROFIT))  # # Compute the average profit
plt.plot(plot3.DAY, plot3.PROFIT)
plt.title('Plot 7 : Average daily profit over 5 years for the initial dataset', fontsize=20)
plt.xlabel("Years", size=14)
plt.ylabel("Profit in euros", size=14)
# plt.savefig("../Results/Plot7.png", bbox_inches='tight')
plt.clf()

# Plot 8 : Average daily profit for the simulation (with the tips)
plot4 = result.groupby(["DAY"])["TOTAMOUNTPAID"].sum().reset_index(name="TOT2")
plot4['DAY'] = pd.to_datetime(plot4['DAY'])  # Declare the time variable
print(statistics.mean(plot4.TOT2))  # # Compute the average profit
plt.plot(plot4.DAY, plot4.TOT2)
plt.title('Plot 8 : Average daily profit including the tip', fontsize=20)
plt.xlabel("Years", size=14)
plt.ylabel("Profit in euros", size=14)
# plt.savefig("../Results/Plot8.png", bbox_inches='tight')
plt.clf()
