# Part 4 - Additional questions

# Import libraries
import pandas as pd
import os
import numpy as np
import random
from pandas import DataFrame
import collections
import statistics
import matplotlib.pyplot as plt
import matplotlib

# Import the initial dataset with years and hours already defined, and our simulation from the previous part
path = os.path.abspath('../Results/export.csv')
df = pd.read_csv(path, sep=";")
path2 = os.path.abspath('../Results/simulation.csv')
simulation = pd.read_csv(path2, sep=";")

# Question 1 : Show some buying histories of returning customers

returning = ['Hipster', 'Regular2']  # List of returning customers' type

# Chose randomly 2 returning customers for whom we will show buying histories
randomreturning = simulation[simulation.SUBTYPE.isin(returning)].ID.sample(2)
history = simulation[simulation.ID.isin(randomreturning)]
print(randomreturning.tolist())  # Display their IDs

# Reorder columns
simulation = simulation[["ID", "HOUR", "DAY", "TIME", "DRINK", "PRICED", "FOOD", "PRICEF", "TYPE", "SUBTYPE", "BUDGET",
                         "AMOUNTPAID", "TIP", "TOTAMOUNTPAID", "CUM_AMOUNTPAID", "BUDGETLEFT"]]

# Create 2 separate buying histories
history1 = simulation[simulation.ID.isin(randomreturning[:1])]
history2 = simulation[simulation.ID.isin(randomreturning[:2])]

# We export both buying histories
# exportpath2 = os.path.abspath('../Results/buying_history1.csv')
# history1.to_csv(exportpath2, sep=";", index=False)
# exportpath3 = os.path.abspath('../Results/buying_history2.csv')
# history2.to_csv(exportpath3, sep=";", index=False)

# Question 2 : Analysis of returning customers of the provided dataset

# Function to display the ID of returning customers coming several times
def duplicates_counter(it):
    return [item for item, count in collections.Counter(it).items() if count > 1]


# Display the ID of returning customers
returning = duplicates_counter(df.CUSTOMER)

print(len(returning))  # Number of returning customers

# Initial dataset with only returning customers
df_returning = df[df.CUSTOMER.isin(returning)]

# Show the specific times when they show up more
pd.set_option('display.max_rows', len(df_returning))  # In order to display the whole observations
print(df_returning['HOUR'].value_counts())  # Count the number of times a customer comes for each time slot
pd.reset_option('display.max_rows')

# Define the types of customers
df['TYPE'] = "One Time"
df.loc[df['CUSTOMER'].isin(returning), 'TYPE'] = "Returning"

# Create a dataframe with the number of all type of customers for each time slot
df_count_type = df.groupby(['HOUR', 'TYPE']).TYPE.count().reset_index(name='OCCURRENCES')
df_count_type = df_count_type.pivot_table('OCCURRENCES', ['HOUR'], 'TYPE')

# Create variable that sums the number of all types of customers for each time slot
df_count_type.loc[:, 'ALL_TYPES'] = df_count_type.sum(numeric_only=True, axis=1)

# Compute the probabilities to having a onetime or returning customer at a given time
df_count_type['PROBABILITY_ONETIME'] = df_count_type['One Time'] / df_count_type['ALL_TYPES']
df_count_type['PROBABILITY_RETURNING'] = df_count_type['Returning'] / df_count_type['ALL_TYPES']
pd.set_option('display.max_rows', None)
print(df_count_type)

# Mean probability to have a one time customer
print(df_count_type.PROBABILITY_ONETIME.mean())
# Mean probability to have a returning customer
print(df_count_type.PROBABILITY_RETURNING.mean())

# Save the probabilities in the export.csv file
# df = df.merge(df_count_type, on='HOUR', how='left')
# exportpath4 = os.path.abspath('../Results/export.csv')
# df.to_csv(exportpath4, sep=";", index=False)

# Correlations between what returning customers buy
correlation_return = df[df.TYPE == "Returning"].groupby(['DRINKS', 'FOOD', 'TYPE']).TYPE.count(). \
    reset_index(name="NUMBER_BOTH").sort_values(['NUMBER_BOTH'], ascending=False)
correlation_return['cum_percent'] = 100 * (
        correlation_return.NUMBER_BOTH.cumsum() / correlation_return.NUMBER_BOTH.sum())
print(correlation_return)

# Correlations between what one timers buy
correlation_onetime = df[df.TYPE == "One Time"].groupby(['DRINKS', 'FOOD', 'TYPE']).TYPE.count().reset_index(
    name="NUMBER_BOTH").sort_values(['NUMBER_BOTH'], ascending=False)
correlation_onetime['cum_percent'] = 100 * (
        correlation_onetime.NUMBER_BOTH.cumsum() / correlation_onetime.NUMBER_BOTH.sum())
print(correlation_onetime)

# Question 3 : The number of returning customers is lowered to 50
# Change in simulation part :
# dfr = pd.DataFrame(dfr["CUSTOMER"].sample(n=50))
# dfh = pd.DataFrame(dfr["CUSTOMER"].sample(n=17))

# Import the simulation
path3 = os.path.abspath('../Results/simulation_question3.csv')
simulation3 = pd.read_csv(path3, sep=";")

# Check if returning customers stop to come at some point
pd.set_option('display.max_rows', None)
simulation3['DAY'] = pd.to_datetime(simulation3['DAY'])
simulation3['MONTH'] = simulation3.DAY.dt.to_period("M")
print(simulation3.groupby(["MONTH", "SUBTYPE"]).DRINK.count())
pd.reset_option('display.max_rows')

# If no returning customers can come replace amount paid, price and choice by NA
simulation3['AMOUNTPAID'] = np.where(simulation3['SUBTYPE'].isnull(), simulation3['SUBTYPE'], simulation3['AMOUNTPAID'])
simulation3['TOTAMOUNTPAID'] = np.where(simulation3['SUBTYPE'].isnull(), simulation3['SUBTYPE'],
                                        simulation3['TOTAMOUNTPAID'])
simulation3['FOOD'] = np.where(simulation3['SUBTYPE'].isnull(), simulation3['SUBTYPE'], simulation3['FOOD'])
simulation3['DRINK'] = np.where(simulation3['SUBTYPE'].isnull(), simulation3['SUBTYPE'], simulation3['DRINK'])
simulation3['PRICED'] = np.where(simulation3['SUBTYPE'].isnull(), simulation3['SUBTYPE'], simulation3['PRICED'])
simulation3['PRICEF'] = np.where(simulation3['SUBTYPE'].isnull(), simulation3['SUBTYPE'], simulation3['PRICEF'])

# Plot 9 : Average daily profit for the simulation 3 (with the tips)
plot1 = simulation3.groupby(["DAY"])["TOTAMOUNTPAID"].sum().reset_index(name="TOT2")
plot1['DAY'] = pd.to_datetime(plot1['DAY'])  # Declare the time variable
print(statistics.mean(plot1.TOT2))  # Compute the average profit
plt.plot(plot1.DAY, plot1.TOT2)
plt.title('Plot 9 : Average daily profit - simulation 3', fontsize=20)
plt.xlabel("Years", size=14)
plt.ylabel("Profit in euros", size=14)
# plt.savefig("../Results/Plot9.png", bbox_inches='tight')
plt.clf()

# Export the result
# exportpath5 = os.path.abspath('../Results/simulation_question3.csv')
# simulation3.to_csv(exportpath5, sep=";", index=False)


# Question 4 : Prices go up by 20% from the beginning of 2018
# Change in theoretical part :
# df.loc[df['DAY'] >= "2018-01-01", 'PRICED'] = (df['PRICED'] * 1.2).round(decimals=2)
# df.loc[df['DAY'] >= "2018-01-01", 'PRICEF'] = (df['PRICEF'] * 1.2).round(decimals=2)
# Change in simulation part :
# maxspendinghip2 = 500 - 9.6
# maxspendingreg2_2 = 250 - 9.6
# TOTAMOUNTPAID = simul1.groupby(['ID','TIME', 'DAY'])["AMOUNTPAID"].sum().groupby(level=0).cumsum().
# reset_index( name="CUM_AMOUNTPAID")
# takeout = TOTAMOUNTPAID[TOTAMOUNTPAID['TOT'] > maxspendinghip] & (TOTAMOUNTPAID['DAY'] < "2017-12-31")]
# takeout2 = TOTAMOUNTPAID[TOTAMOUNTPAID['TOT'] > maxspendingreg2]  & (TOTAMOUNTPAID['DAY'] < "2017-12-31"])
# takeout2 = TOTAMOUNTPAID[(TOTAMOUNTPAID['CUM_AMOUNTPAID'] > maxspendinghip2)&(TOTAMOUNTPAID['DAY'] >= "2017-12-31")]
# takeout2 = takeout2['ID'].tolist()
# for id2 in takeout2:
# if id2 in colh:
# colh.remove(id2)
# takeout4 = TOTAMOUNTPAID[(TOTAMOUNTPAID['CUM_AMOUNTPAID'] > maxspendingreg2_2) &
# (TOTAMOUNTPAID['DAY'] >= "2017-12-31")]
# takeout4 = takeout4['ID'].tolist()
# for id4 in takeout4:
# if id4 in colreg2:
# colreg2.remove(id4)

# Import the simulation
path4 = os.path.abspath('../Results/simulation_question4.csv')
simulation4 = pd.read_csv(path4, sep=";")

# Check if returning customers stop to come at some point
pd.set_option('display.max_rows', None)
simulation4['DAY'] = pd.to_datetime(simulation4['DAY'])
simulation4['MONTH'] = simulation4.DAY.dt.to_period("M")
print(simulation4.groupby(["MONTH", "SUBTYPE"]).DRINK.count())
pd.reset_option('display.max_rows')

# Plot 10 : Average daily profit for the simulation 4 (with the tips)
plot2 = simulation4.groupby(["DAY"])["TOTAMOUNTPAID"].sum().reset_index(name="TOT2")
plot2['DAY'] = pd.to_datetime(plot2['DAY'])  # Declare the time variable
print(statistics.mean(plot2.TOT2))  # average profit
plt.plot(plot2.DAY, plot2.TOT2)
plt.title('Plot 10 : Average daily profit - simulation 4', fontsize=20)
plt.xlabel("Years", size=14)
plt.ylabel("Profit in euros", size=14)
# plt.savefig("../Results/Plot10.png", bbox_inches='tight')
plt.clf()

# Question 5 : The budget of hipsters drops to 40
# Change in simulation part :
# maxspendinghip = 40-8
# simul1.loc[simul1['SUBTYPE'] == 'Hipster', "BUDGET"] = 40

# Import the simulation
path5 = os.path.abspath('../Results/simulation_question5.csv')
simulation5 = pd.read_csv(path5, sep=";")

# Check if returning customers stop to come at some point
pd.set_option('display.max_rows', None)
simulation5['DAY'] = pd.to_datetime(simulation5['DAY'])
simulation5['MONTH'] = simulation5.DAY.dt.to_period("M")
print(simulation5.groupby(["MONTH", "SUBTYPE"]).DRINK.count())
pd.reset_option('display.max_rows')

# If no returning customers can come replace amount paid, price and choice by NA
simulation5['AMOUNTPAID'] = np.where(simulation5['SUBTYPE'].isnull(), simulation5['SUBTYPE'], simulation5['AMOUNTPAID'])
simulation5['TOTAMOUNTPAID'] = np.where(simulation5['SUBTYPE'].isnull(), simulation5['SUBTYPE'],
                                        simulation5['TOTAMOUNTPAID'])
simulation5['FOOD'] = np.where(simulation5['SUBTYPE'].isnull(), simulation5['SUBTYPE'], simulation5['FOOD'])
simulation5['DRINK'] = np.where(simulation5['SUBTYPE'].isnull(), simulation5['SUBTYPE'], simulation5['DRINK'])
simulation5['PRICED'] = np.where(simulation5['SUBTYPE'].isnull(), simulation5['SUBTYPE'], simulation5['PRICED'])
simulation5['PRICEF'] = np.where(simulation5['SUBTYPE'].isnull(), simulation5['SUBTYPE'], simulation5['PRICEF'])

# Plot 11 : Average daily profit for the simulation 5 (with the tips)
plot3 = simulation5.groupby(["DAY"])["TOTAMOUNTPAID"].sum().reset_index(name="TOT2")
plot3['DAY'] = pd.to_datetime(plot3['DAY'])  # Declare the time variable
print(statistics.mean(plot3.TOT2))  # # Compute the average profit
plt.plot(plot3.DAY, plot3.TOT2)
plt.title('Plot 11 : Average daily profit - simulation 5', fontsize=20)
plt.xlabel("Years", size=14)
plt.ylabel("Profit in euros", size=14)
# plt.savefig("../Results/Plot11.png", bbox_inches='tight')
plt.clf()

# Export the result
# exportpath6 = os.path.abspath('../Results/simulation_question5.csv')
# simulation5.to_csv(exportpath6, sep=";", index=False)


# Question 6 : The proportion of returning and one time consumers is modified to 50% of returning and 50% of onetimers
# Change in simulation part :
# gen = pd.DataFrame(np.random.choice(['Return', 'Onetime'], simul, p=[0.5, 0.5]), columns=['TYPE'])

# Import the simulation
path6 = os.path.abspath('../Results/simulation_question6.csv')
simulation6 = pd.read_csv(path6, sep=";")

# Check if returning customers stop to come at some point
pd.set_option('display.max_rows', None)
simulation6['DAY'] = pd.to_datetime(simulation6['DAY'])
simulation6['MONTH'] = simulation6.DAY.dt.to_period("M")
print(simulation6.groupby(["MONTH", "SUBTYPE"]).DRINK.count())
pd.reset_option('display.max_rows')

# If no returning customers can come replace amount paid, price and choice by NA
simulation6['AMOUNTPAID'] = np.where(simulation6['SUBTYPE'].isnull(), simulation6['SUBTYPE'], simulation6['AMOUNTPAID'])
simulation6['TOTAMOUNTPAID'] = np.where(simulation6['SUBTYPE'].isnull(), simulation6['SUBTYPE'],
                                        simulation6['TOTAMOUNTPAID'])
simulation6['FOOD'] = np.where(simulation6['SUBTYPE'].isnull(), simulation6['SUBTYPE'], simulation6['FOOD'])
simulation6['DRINK'] = np.where(simulation6['SUBTYPE'].isnull(), simulation6['SUBTYPE'], simulation6['DRINK'])
simulation6['PRICED'] = np.where(simulation6['SUBTYPE'].isnull(), simulation6['SUBTYPE'], simulation6['PRICED'])
simulation6['PRICEF'] = np.where(simulation6['SUBTYPE'].isnull(), simulation6['SUBTYPE'], simulation6['PRICEF'])

# Plot 12 : Average daily profit for the simulation 6 (with the tips)
plot4 = simulation6.groupby(["DAY"])["TOTAMOUNTPAID"].sum().reset_index(name="TOT2")
plot4['DAY'] = pd.to_datetime(plot3['DAY'])  # Declare the time variable
print(statistics.mean(plot4.TOT2))  # Compute the average profit
plt.plot(plot4.DAY, plot4.TOT2)
plt.title('Plot 12 : Average daily profit - simulation 6', fontsize=20)
plt.xlabel("Years", size=14)
plt.ylabel("Profit in euros", size=14)
# plt.savefig("../Results/Plot12.png", bbox_inches='tight')
plt.clf()

# Export the result
# exportpath7 = os.path.abspath('../Results/simulation_question6.csv')
# simulation6.to_csv(exportpath7, sep=";", index=False)
