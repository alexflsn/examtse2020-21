# Part 2 : Theoretical file

# Import libraries
import pandas as pd
import os
import numpy as np
import random
from pandas import DataFrame

# Import the probabilities found in Part 1
exportpath = os.path.abspath('../Results/export.csv')
export = pd.read_csv(exportpath, sep=";")

# Initialize some lists
dr = []
dr1 = []

# Define the probabilities as integer
export[['PROBABILITY_COFFEE', 'PROBABILITY_TEA', 'PROBABILITY_WATER', "PROBABILITY_MILKSHAKE",
        "PROBABILITY_FRAPPUCCINO", "PROBABILITY_SODA", "PROBABILITY_PIE", "PROBABILITY_MUFFIN", "PROBABILITY_COOKIE",
        "PROBABILITY_SANDWICH", "PROBABILITY_NOTHING"]] = export[
    ['PROBABILITY_COFFEE', 'PROBABILITY_TEA', 'PROBABILITY_WATER', "PROBABILITY_MILKSHAKE", "PROBABILITY_FRAPPUCCINO",
     "PROBABILITY_SODA", "PROBABILITY_PIE", "PROBABILITY_MUFFIN", "PROBABILITY_COOKIE", "PROBABILITY_SANDWICH",
     "PROBABILITY_NOTHING"]].astype(int)


# Creation of customer class
class Customer(object):
    def __init__(self, inputid, inputbudget):
        # Initialization of the attributes
        self.id = inputid
        self.budget = inputbudget

    # Creation of the description model
    def description(self):
        print("This is customer %s, with a budget of %s €" % (self.id, self.budget))

    # Given probabilities, find drink and food chosen at hour i

    # Choice of the drink given the probabilities
    def probabilities(self):
        for i in range(len(export)):
            drink = ['coffee'] * export.loc[i, "PROBABILITY_COFFEE"] + ['tea'] * export.loc[i, "PROBABILITY_TEA"] + [
                'water'] \
                    * export.loc[i, "PROBABILITY_WATER"] + ['milkshake'] * export.loc[i, "PROBABILITY_MILKSHAKE"] + \
                    ['frappuccino'] * export.loc[i, "PROBABILITY_FRAPPUCCINO"] + ['soda'] * export.loc[
                        i, "PROBABILITY_SODA"]
            dr.append(random.choice(drink))

        # Create a dataframe with the drinks
        df = pd.DataFrame(dr, columns=['DRINK'])

        # Add the hours and the day
        selected_columns = export["HOUR"]
        selected_columns2 = export["DAY"]
        selected_columns3 = export["TIME"]
        df['HOUR'] = selected_columns.copy()
        df['DAY'] = selected_columns2.copy()
        df['TIME'] = selected_columns3.copy()

        # Add the prices of the drinks with the new column 'PRICED'
        def set_value(row_number, assigned_value):
            return assigned_value[row_number]

        drink_dictionary = {'milkshake': 5, 'frappuccino': 4, 'water': 2,
                            'coffee': 3, 'soda': 3, 'tea': 3}

        df['PRICED'] = df['DRINK'].apply(set_value, args=(drink_dictionary,))
        # Prices go up by 20% from the beginning of 2018
        # df.loc[df['DAY'] >= "2018-01-01", 'PRICED'] = (df['PRICED'] * 1.2).round(decimals=2)

        # Choice of the food given the probabilities
        for i in range(len(export)):
            food = ['cookie'] * export.loc[i, "PROBABILITY_COOKIE"] + ['muffin'] * export.loc[
                i, "PROBABILITY_MUFFIN"] + [
                       'pie'] * \
                   export.loc[i, "PROBABILITY_PIE"] + ['sandwich'] * export.loc[i, "PROBABILITY_SANDWICH"] + [
                       'nothing'] * export.loc[i, "PROBABILITY_NOTHING"]
            dr1.append(random.choice(food))

        # Create a dataframe with the food and merge it with df
        dff = pd.DataFrame(dr1, columns=['FOOD'])
        selected_columns1 = dff["FOOD"]
        df['FOOD'] = selected_columns1.copy()

        # Add the prices of the food with the new column 'PRICEF'
        def set_value(row_number, assigned_value):
            return assigned_value[row_number]

        food_dictionary = {'sandwich': 2, 'cookie': 2, 'muffin': 3, 'pie': 3,
                           'nothing': 0}

        df['PRICEF'] = df['FOOD'].apply(set_value, args=(food_dictionary,))
        # Prices go up by 20% from the beginning of 2018
        # df.loc[df['DAY'] >= "2018-01-01", 'PRICEF'] = (df['PRICEF'] * 1.2).round(decimals=2)

        # Print sentence
        # print("At " + df.HOUR + " customer " + self.id + " buys " + df.DRINK + " and " + df.FOOD + " to eat ")

        # Define the amount paid
        df['AMOUNTPAID'] = df.PRICEF + df.PRICED
        df['DAY'] = df['DAY'].astype(str)
        return df
