# Part 2 : Theoretical file

# Import libraries
import pandas as pd
import os
import numpy as np
import random
from pandas import DataFrame

# Import the probabilities found in Part 1 and the initial dataset
path1 = os.path.abspath("../Results/Probabilities_Drinks.csv")
df1 = pd.read_csv(path1, sep=";")
path2 = os.path.abspath("../Results/Probabilities_Food.csv")
df2 = pd.read_csv(path2, sep=";")
path = os.path.abspath("../Data/Coffeebar_2016-2020.csv")

# Initialize some lists
dr = []
dr1 = []

# Initialize the number of days on which we simulate
simul = 365 * 5 * 171  # 5 years

# Initialize the prices (taken from part 3 instructions)
psandwich = 2
pcookie = 2
ppie = 3
pmuffin = 3
pnothing = 0

pmilkshake = 5
pfrappuccino = 4
pwater = 2
pcoffee = 3
ptea = 3
psoda = 3

# Define the probabilities as integer
df1["PROBABILITY_COFFEE"] = df1["PROBABILITY_COFFEE"].apply(np.int64)
df1["PROBABILITY_TEA"] = df1["PROBABILITY_TEA"].apply(np.int64)
df1["PROBABILITY_WATER"] = df1["PROBABILITY_WATER"].apply(np.int64)
df1["PROBABILITY_MILKSHAKE"] = df1["PROBABILITY_MILKSHAKE"].apply(np.int64)
df1["PROBABILITY_FRAPPUCCINO"] = df1["PROBABILITY_FRAPPUCCINO"].apply(np.int64)
df1["PROBABILITY_SODA"] = df1["PROBABILITY_SODA"].apply(np.int64)

df2["PROBABILITY_PIE"] = df2["PROBABILITY_PIE"].apply(np.int64)
df2["PROBABILITY_MUFFIN"] = df2["PROBABILITY_MUFFIN"].apply(np.int64)
df2["PROBABILITY_COOKIE"] = df2["PROBABILITY_COOKIE"].apply(np.int64)
df2["PROBABILITY_SANDWICH"] = df2["PROBABILITY_SANDWICH"].apply(np.int64)
df2["PROBABILITY_NOTHING"] = df2["PROBABILITY_NOTHING"].apply(np.int64)


# Creation of customer class
class Customer(object):
    def __init__(self, inputid, inputbudget):
        # Initialization of the attributes
        self.id = inputid
        self.budget = inputbudget

    # Creation of the description model
    def description(self):
        print("This is customer %s, with a budget of %s €" % (self.id, self.budget))


# Creation of subclasses
class Onetime(Customer):
    def __init__(self, inputid, inputbudget=100):
        super().__init__(inputid, inputbudget)
        self.id = inputid
        self.budget = inputbudget


class Return(Customer):
    def __init__(self, inputid, inputbudget):
        super().__init__(inputid, inputbudget)
        self.id = inputid
        self.budget = inputbudget


# Creation of sub-subclasses
class Regular1(Onetime):
    def __init__(self, inputid, inputhour, inputday, inputbudget=100):
        super().__init__(inputid, inputbudget)
        self.id = inputid
        self.budget = inputbudget
        self.hour = inputhour
        self.day = inputday

    # Given probabilities, find drink and food chosen at hour i

    # Choice of the drink given the probabilities
    def probabilities(self):
        for n in range(0, simul):
            for i in range(len(df1)):
                drink = ['coffee'] * df1.loc[i, "PROBABILITY_COFFEE"] + ['tea'] * df1.loc[i, "PROBABILITY_TEA"] + [
                    'water'] \
                        * df1.loc[i, "PROBABILITY_WATER"] + ['milkshake'] * df1.loc[i, "PROBABILITY_MILKSHAKE"] + \
                        ['frappuccino'] * df1.loc[i, "PROBABILITY_FRAPPUCCINO"] + ['soda'] * df1.loc[
                            i, "PROBABILITY_SODA"]
                dr.append(random.choice(drink))

            # Create a dataframe with the drinks
            df = pd.DataFrame(dr, columns=['DRINK'])

            # Add the hours and the day
            selected_columns = df1["HOUR"].tolist()
            df['HOUR'] = np.resize(selected_columns, len(df))
            df['DAY'] = (df.index / 171).astype(int)

            # Add the prices of the drinks with the new column 'PRICED'
            def set_value(row_number, assigned_value):
                return assigned_value[row_number]

            drink_dictionary = {'milkshake': pmilkshake, 'frappuccino': pfrappuccino, 'water': pwater,
                                'coffee': pcoffee, 'soda': psoda, 'tea': ptea}

            df['PRICED'] = df['DRINK'].apply(set_value, args=(drink_dictionary,))

            # Choice of the food given the probabilities
            for i in range(len(df2)):
                food = ['cookie'] * df2.loc[i, "PROBABILITY_COOKIE"] + ['muffin'] * df2.loc[i, "PROBABILITY_MUFFIN"] + [
                    'pie'] * \
                       df2.loc[i, "PROBABILITY_PIE"] + ['sandwich'] * df2.loc[i, "PROBABILITY_SANDWICH"] + [
                           'nothing'] * df2.loc[i, "PROBABILITY_NOTHING"]
                dr1.append(random.choice(food))

        # Create a dataframe with the food and merge it with df
        dff = pd.DataFrame(dr1, columns=['FOOD'])
        selected_columns1 = dff["FOOD"]
        df['FOOD'] = selected_columns1.copy()

        # Add the prices of the food with the new column 'PRICEF'
        def set_value(row_number, assigned_value):
            return assigned_value[row_number]

        food_dictionary = {'sandwich': psandwich, 'cookie': pcookie, 'muffin': pmuffin, 'pie': ppie,
                           'nothing': pnothing}

        df['PRICEF'] = df['FOOD'].apply(set_value, args=(food_dictionary,))

        # Print sentence
        # print("At " + df.HOUR + " customer " + self.id + " buys " + df.DRINK + " and " + df.FOOD + " to eat ")

        # Define the amount paid
        df['AMOUNT PAID'] = df.PRICEF + df.PRICED
        df['ID'] = self.id
        df['DAY'] = df['DAY'].astype(str)
        v = df[(df.HOUR == self.hour) & (df.DAY == self.day)]
        return v


class Tripadvisor(Onetime):
    def __init__(self, inputid, inputhour, inputday, inputbudget=100):
        super().__init__(inputid, inputbudget)
        self.id = inputid
        self.budget = inputbudget
        self.hour = inputhour
        self.day = inputday

    # Given probabilities, find drink and food chosen at hour i

    # Choice of the food given the probabilities
    def probabilities(self):
        for n in range(0, simul):
            for i in range(len(df1)):
                drink = ['coffee'] * df1.loc[i, "PROBABILITY_COFFEE"] + ['tea'] * df1.loc[i, "PROBABILITY_TEA"] + [
                    'water'] \
                        * df1.loc[i, "PROBABILITY_WATER"] + ['milkshake'] * df1.loc[i, "PROBABILITY_MILKSHAKE"] + \
                        ['frappuccino'] * df1.loc[i, "PROBABILITY_FRAPPUCCINO"] + ['soda'] * df1.loc[
                            i, "PROBABILITY_SODA"]
                dr.append(random.choice(drink))

            # Create a dataframe with the drinks
            df = pd.DataFrame(dr, columns=['DRINK'])

            # Add the hours and the day
            selected_columns = df1["HOUR"].tolist()
            df['HOUR'] = np.resize(selected_columns, len(df))
            df['DAY'] = (df.index / 171).astype(int)

            # Add the prices of the drinks with the new column 'PRICED'
            def set_value(row_number, assigned_value):
                return assigned_value[row_number]

            drink_dictionary = {'milkshake': pmilkshake, 'frappuccino': pfrappuccino, 'water': pwater,
                                'coffee': pcoffee, 'soda': psoda, 'tea': ptea}

            df['PRICED'] = df['DRINK'].apply(set_value, args=(drink_dictionary,))

            # Choice of the food given the probabilities
            for i in range(len(df2)):
                food = ['cookie'] * df2.loc[i, "PROBABILITY_COOKIE"] + ['muffin'] * df2.loc[
                    i, "PROBABILITY_MUFFIN"] + [
                           'pie'] * \
                       df2.loc[i, "PROBABILITY_PIE"] + ['sandwich'] * df2.loc[i, "PROBABILITY_SANDWICH"] + [
                           'nothing'] * df2.loc[i, "PROBABILITY_NOTHING"]
                dr1.append(random.choice(food))

        # Create a dataframe with the food and merge it with df
        dff = pd.DataFrame(dr1, columns=['FOOD'])
        selected_columns1 = dff["FOOD"]
        df['FOOD'] = selected_columns1.copy()

        # Add the prices of the food with the new column 'PRICEF'
        def set_value(row_number, assigned_value):
            return assigned_value[row_number]

        food_dictionary = {'sandwich': psandwich, 'cookie': pcookie, 'muffin': pmuffin, 'pie': ppie,
                           'nothing': pnothing}

        df['PRICEF'] = df['FOOD'].apply(set_value, args=(food_dictionary,))

        # Print sentence
        # print("At " + df.HOUR + " customer " + self.id + " buys " + df.DRINK + " and " + df.FOOD + " to eat ")

        # Define the amount and the random tip paid
        df['TIP'] = round(random.uniform(1, 10), 0)
        df['AMOUNT PAID'] = df.PRICEF + df.PRICED + df.TIP
        df['ID'] = self.id
        df['DAY'] = df['DAY'].astype(str)
        v = df[(df.HOUR == self.hour) & (df.DAY == self.day)]
        return v


class Regular2(Return):
    def __init__(self, inputid, inputhour, inputday, inputbudget=250):
        super().__init__(inputid, inputbudget)
        self.id = inputid
        self.budget = inputbudget
        self.hour = inputhour
        self.day = inputday

    # Given probabilities, find drink and food chosen at hour i

    # Choice of the food given the probabilities
    def probabilities(self):
        for n in range(0, simul):
            for i in range(len(df1)):
                drink = ['coffee'] * df1.loc[i, "PROBABILITY_COFFEE"] + ['tea'] * df1.loc[i, "PROBABILITY_TEA"] + [
                    'water'] \
                        * df1.loc[i, "PROBABILITY_WATER"] + ['milkshake'] * df1.loc[i, "PROBABILITY_MILKSHAKE"] + \
                        ['frappuccino'] * df1.loc[i, "PROBABILITY_FRAPPUCCINO"] + ['soda'] * df1.loc[
                            i, "PROBABILITY_SODA"]
                dr.append(random.choice(drink))

            # Create a dataframe with the drinks
            df = pd.DataFrame(dr, columns=['DRINK'])

            # Add the hours and the day
            selected_columns = df1["HOUR"].tolist()
            df['HOUR'] = np.resize(selected_columns, len(df))
            df['DAY'] = (df.index / 171).astype(int)

            # Add the prices of the drinks with the new column 'PRICED'
            def set_value(row_number, assigned_value):
                return assigned_value[row_number]

            drink_dictionary = {'milkshake': pmilkshake, 'frappuccino': pfrappuccino, 'water': pwater,
                                'coffee': pcoffee, 'soda': psoda, 'tea': ptea}

            df['PRICED'] = df['DRINK'].apply(set_value, args=(drink_dictionary,))

            # Choice of the food given the probabilities
            for i in range(len(df2)):
                food = ['cookie'] * df2.loc[i, "PROBABILITY_COOKIE"] + ['muffin'] * df2.loc[
                    i, "PROBABILITY_MUFFIN"] + [
                           'pie'] * \
                       df2.loc[i, "PROBABILITY_PIE"] + ['sandwich'] * df2.loc[i, "PROBABILITY_SANDWICH"] + [
                           'nothing'] * df2.loc[i, "PROBABILITY_NOTHING"]
                dr1.append(random.choice(food))

        # Create a dataframe with the food and merge it with df
        dff = pd.DataFrame(dr1, columns=['FOOD'])
        selected_columns1 = dff["FOOD"]
        df['FOOD'] = selected_columns1.copy()

        # Add the prices of the food with the new column 'PRICEF'
        def set_value(row_number, assigned_value):
            return assigned_value[row_number]

        food_dictionary = {'sandwich': psandwich, 'cookie': pcookie, 'muffin': pmuffin, 'pie': ppie,
                           'nothing': pnothing}

        df['PRICEF'] = df['FOOD'].apply(set_value, args=(food_dictionary,))

        # Print sentence
        # print("At " + df.HOUR + " customer " + self.id + " buys " + df.DRINK + " and " + df.FOOD + " to eat ")

        # Define the amount paid
        df['AMOUNT PAID'] = df.PRICEF + df.PRICED
        df['ID'] = self.id
        df['DAY'] = df['DAY'].astype(str)
        v = df[(df.HOUR == self.hour) & (df.DAY == self.day)]
        return v


class Hipsters(Return):
    def __init__(self, inputid, inputhour, inputday, inputbudget=500):
        super().__init__(inputid, inputbudget)
        self.id = inputid
        self.budget = inputbudget
        self.hour = inputhour
        self.day = inputday

    # Given probabilities, find drink and food chosen at hour i

    # Choice of the food given the probabilities
    def probabilities(self):
        for n in range(0, simul):
            for i in range(len(df1)):
                drink = ['coffee'] * df1.loc[i, "PROBABILITY_COFFEE"] + ['tea'] * df1.loc[i, "PROBABILITY_TEA"] + [
                    'water'] \
                        * df1.loc[i, "PROBABILITY_WATER"] + ['milkshake'] * df1.loc[i, "PROBABILITY_MILKSHAKE"] + \
                        ['frappuccino'] * df1.loc[i, "PROBABILITY_FRAPPUCCINO"] + ['soda'] * df1.loc[
                            i, "PROBABILITY_SODA"]
                dr.append(random.choice(drink))

            # Create a dataframe with the drinks
            df = pd.DataFrame(dr, columns=['DRINK'])

            # Add the hours and the day
            selected_columns = df1["HOUR"].tolist()
            df['HOUR'] = np.resize(selected_columns, len(df))
            df['DAY'] = (df.index / 171).astype(int)

            # Add the prices of the drinks with the new column 'PRICED'
            def set_value(row_number, assigned_value):
                return assigned_value[row_number]

            drink_dictionary = {'milkshake': pmilkshake, 'frappuccino': pfrappuccino, 'water': pwater,
                                'coffee': pcoffee, 'soda': psoda, 'tea': ptea}

            df['PRICED'] = df['DRINK'].apply(set_value, args=(drink_dictionary,))

            # Choice of the food given the probabilities
            for i in range(len(df2)):
                food = ['cookie'] * df2.loc[i, "PROBABILITY_COOKIE"] + ['muffin'] * df2.loc[
                    i, "PROBABILITY_MUFFIN"] + [
                           'pie'] * \
                       df2.loc[i, "PROBABILITY_PIE"] + ['sandwich'] * df2.loc[i, "PROBABILITY_SANDWICH"] + [
                           'nothing'] * df2.loc[i, "PROBABILITY_NOTHING"]
                dr1.append(random.choice(food))

        # Create a dataframe with the food and merge it with df
        dff = pd.DataFrame(dr1, columns=['FOOD'])
        selected_columns1 = dff["FOOD"]
        df['FOOD'] = selected_columns1.copy()

        # Add the prices of the food with the new column 'PRICEF'
        def set_value(row_number, assigned_value):
            return assigned_value[row_number]

        food_dictionary = {'sandwich': psandwich, 'cookie': pcookie, 'muffin': pmuffin, 'pie': ppie,
                           'nothing': pnothing}

        df['PRICEF'] = df['FOOD'].apply(set_value, args=(food_dictionary,))

        # Print sentence
        # print("At " + df.HOUR + " customer " + self.id + " buys " + df.DRINK + " and " + df.FOOD + " to eat ")

        # Define the amount paid
        df['AMOUNT PAID'] = df.PRICEF + df.PRICED
        df['ID'] = self.id
        df['DAY'] = df['DAY'].astype(str)
        v = df[(df.HOUR == self.hour) & (df.DAY == self.day)]
        return v
