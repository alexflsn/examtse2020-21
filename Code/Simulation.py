# Part 3 : Simulations

# Section 1 : The simulation

# Import the theoretical file
from Theoretical import Customer

# Import libraries
import time
import pandas as pd
import os
import numpy as np
import random
from pandas import DataFrame
import collections
import matplotlib.pyplot as plt
import matplotlib
from random import sample

# Initialize some lists
colreg1 = []
colh = []
tip = []
col = []

# Maximum total amount spent for hipster
maxspendinghip = 500 - 8
# The budget of hipsters drops to 40
# maxspendinghip = 40 - 8
# Prices go up by 20% from the beginning of 2018
# maxspendinghip2 = 500 - 9.6

# Maximum total amount spent for regular2
maxspendingreg2 = 250 - 8
# Prices go up by 20% from the beginning of 2018
# maxspendingreg2_2 = 250 - 9.6

# Initialize the number of slots (day+hour) on which we simulate
simul = 365 * 5 * 171  # 5 years simulation

# Import the initial dataset and data set with drink probabilities
exportpath = os.path.abspath('../Results/export.csv')
export = pd.read_csv(exportpath, sep=";")

# Obtain the list of the different customer ID
coldf = export['CUSTOMER'].tolist()


# Choose IDs of the returning customer based on the initial dataset
def duplicates_counter(it):
    return [item for item, count in collections.Counter(it).items() if count > 1]


dfr = pd.DataFrame(duplicates_counter(export.CUSTOMER), columns=["CUSTOMER"])
# The number of returning customers is lowered to 50
# dfr = pd.DataFrame(dfr["CUSTOMER"].sample(n=50))

# List of ids of the returning customer
colr = dfr['CUSTOMER'].tolist()

# Choose among ids of returning customers the ones for hipster customers
dfh = pd.DataFrame(dfr["CUSTOMER"].sample(n=334))
# The number of returning customers is lowered to 50
# dfh = pd.DataFrame(dfr["CUSTOMER"].sample(n=17))

colh = dfh['CUSTOMER'].tolist()  # List of ids of hipster customers

# Choose the remaining ids for the regular2 customers
reg2 = dfr[~dfr.index.isin(dfh.index)]
colreg2 = reg2['CUSTOMER'].tolist()  # List of ids of regular2 customers

# Simulate the probabilities
simul1 = Customer("id", "budget")
simul1 = simul1.probabilities()

# For each hour of each day, determine which type of customer is coming to the coffeebar
# We randomly choose customers, 20% of returning, 80% of one time
gen = pd.DataFrame(np.random.choice(['Return', 'Onetime'], simul, p=[0.2, 0.8]), columns=['TYPE'])
# The proportion of returning and one time consumers is modified to 50%-50%
# gen = pd.DataFrame(np.random.choice(['Return', 'Onetime'], simul, p=[0.5, 0.5]), columns=['TYPE'])
type = gen['TYPE'].tolist()
simul1['TYPE'] = type
Index_label = simul1[simul1['TYPE'] == 'Return'].index.tolist()

# Determine the subtypes for returning customers
for val, i in enumerate(Index_label):
    li = colh + colreg2
    if len(li) != 0:  # If there is still hipsters or regular2 that have enough budget
        TYPER = random.choice(colreg2 + colh)  # Random choice between regular2 and hipster

    if TYPER in colh:
        simul1.loc[i, "SUBTYPE"] = "Hipster"
        simul1.loc[i, "ID"] = TYPER

    if TYPER in colreg2:
        simul1.loc[i, "SUBTYPE"] = "Regular2"
        simul1.loc[i, "ID"] = TYPER

    # By ID, compute the total amount paid until today
    TOTAMOUNTPAID = simul1.groupby(["ID"])["AMOUNTPAID"].sum().reset_index(name="TOT")

    # Take out the Hipster customers who do not have enough budget
    takeout = TOTAMOUNTPAID[TOTAMOUNTPAID['TOT'] > maxspendinghip]  # &(TOTAMOUNTPAID['DAY'] < "2017-12-31")]
    takeout = takeout['ID'].tolist()
    for id1 in takeout:
        if id1 in colh:
            colh.remove(id1)

    # Prices go up by 20% from the beginning of 2018
    # takeout2 = TOTAMOUNTPAID[(TOTAMOUNTPAID['CUM_AMOUNTPAID'] > maxspendinghip2)&(TOTAMOUNTPAID['DAY'] >=
    # "2017-12-31")] takeout2 = takeout2['ID'].tolist() for id2 in takeout2: if id2 in colh: colh.remove(id2)

    # Take out the Regular2 customers who do not have enough budget
    takeout2 = TOTAMOUNTPAID[TOTAMOUNTPAID['TOT'] > maxspendingreg2]  # & (TOTAMOUNTPAID['DAY'] < "2017-12-31"
    takeout2 = takeout2['ID'].tolist()
    for id2 in takeout2:
        if id2 in colreg2:
            colreg2.remove(id2)

    # Prices go up by 20% from the beginning of 2018
    # takeout4 = TOTAMOUNTPAID[(TOTAMOUNTPAID['CUM_AMOUNTPAID'] > maxspendingreg2_2) & (TOTAMOUNTPAID['DAY'] >=
    # "2017-12-31")] takeout4 = takeout4['ID'].tolist() for id4 in takeout4: if id4 in colreg2: colreg2.remove(id4)


# Determine the id of the one time customers
# Generate IDs
def running_id():
    n1 = 1
    while True:
        yield 'OT{0:05d}'.format(n1)
        n1 += 1


C = running_id()
numbert = len(simul1[simul1['TYPE'] == 'Onetime'])  # Count the number of one time customers
for n in range(numbert):  # Generate as much as ID as there is one time customers
    col.append(next(C))
simul1.loc[simul1['TYPE'] == 'Onetime', "ID"] = col  # Put the ID in the ID column of the dataframe

# Determine who comes
subtype = np.random.choice(['Tripadvisor', 'Regular1'], numbert, p=[0.1, 0.9])
simul1.loc[simul1['TYPE'] == 'Onetime', "SUBTYPE"] = subtype

# Count the number of tripadvisor customers
numbertrip = len(simul1[simul1['SUBTYPE'] == 'Tripadvisor'])

# Generate a list of random tip for the tripadvisor customers
for i in range(0, numbertrip):
    tip.append(random.randint(1, 10))

# Add the Tip between 1 and 10 for Tripadvisor customer
simul1.loc[simul1['SUBTYPE'] == 'Tripadvisor', "TIP"] = tip

# Replacing na values in tip with 0
simul1['TIP'].fillna(0, inplace=True)

# Add the tip to the amount paid
simul1["TOTAMOUNTPAID"] = simul1["AMOUNTPAID"] + simul1["TIP"]

# Add a column for the initial budget
simul1.loc[simul1['SUBTYPE'] == 'Tripadvisor', "BUDGET"] = 100
simul1.loc[simul1['SUBTYPE'] == 'Regular1', "BUDGET"] = 100
simul1.loc[simul1['SUBTYPE'] == 'Regular2', "BUDGET"] = 250
simul1.loc[simul1['SUBTYPE'] == 'Hipster', "BUDGET"] = 500
# The budget of hipsters drops to 40
# simul1.loc[simul1['SUBTYPE'] == 'Hipster', "BUDGET"] = 40

# Cumulated sum of total amount paid by ID
cumulated = simul1.groupby(['ID', 'TIME'])["TOTAMOUNTPAID"].sum().groupby(level=0).cumsum().reset_index(
    name="CUM_AMOUNTPAID")

# Add a column for the cumulated sum
simul1 = pd.merge(simul1, cumulated, how='left', on=['ID', 'TIME'])

# Add a column with the budget left
simul1["BUDGETLEFT"] = simul1["BUDGET"] - simul1["CUM_AMOUNTPAID"]

# RUN ONLY FOR PART 3 - SIMULATION
# exportpath1 = os.path.abspath('../Results/simulation.csv')
# simul1.to_csv(exportpath1, sep=";", index=False)

# RUN ONLY FOR PART 4 - QUESTION 3
# exportpath3 = os.path.abspath('../Results/simulation_question3.csv')
# simul1.to_csv(exportpath3, sep=";", index=False)

# RUN ONLY FOR PART 4 - QUESTION 4
# exportpath4 = os.path.abspath('../Results/simulation_question4.csv')
# simul1.to_csv(exportpath4, sep=";", index=False)

# RUN ONLY FOR PART 4 - QUESTION 5
# exportpath5 = os.path.abspath('../Results/simulation_question5.csv')
# simul1.to_csv(exportpath5, sep=";", index=False)

# RUN ONLY FOR PART 4 - QUESTION 6
# exportpath5 = os.path.abspath('../Results/simulation_question6.csv')
# simul1.to_csv(exportpath5, sep=";", index=False)
